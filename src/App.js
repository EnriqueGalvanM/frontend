import React from 'react';
import './App.css';
import Home from './components/Pages/Home'
import History from './components/Pages/History'
import Sports from './components/Pages/Sports'
import Matches from './components/Pages/Matches'
import Gallery from './components/Pages/Gallery'
import Social from './components/Pages/Social'
import Login from './components/Pages/Login'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import Loading from './components/Loading/Loading'
import Backg from './components/Video/Video'
import {Spinner} from 'reactstrap'
import LoadPage from './components/Loading/LoadPage'
import Countries from './components/Pages/Countries'

export default class App extends React.Component {
  state = {
    loaded: false
  }
  constructor(){
    super()
    Loading.load(() => this.setState({loaded: true}))
  }
  render(){
    return (
      <div className="App">
        <Router>
            <Switch>
            {this.state.loaded ?
              <Route
                path="/"
                exact
                render={props => <Backg />}
              /> : <LoadPage></LoadPage>
            }
             <Route
                path="/home"
                exact
                render={props => <Home />}
              />
              <Route
                path="/historia"
                exact
                render={props => <History/>}
              />
              <Route
                path="/sports"
                exact
                render={props => <Sports/>}
              />
                        <Route
                path="/partidos"
                exact
                render={props => <Matches/>}
              />
                        <Route
                path="/galeria"
                exact
                render={props => <Gallery/>}
              />
                        <Route
                path="/countries"
                exact
                render={props => <Countries/>}
              />
                        <Route
                path="/login"
                exact
                render={props => <Login/>}
              />
              
            </Switch>
          </Router>
  
      
      </div>
    );
  }
}

