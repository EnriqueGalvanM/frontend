import React from 'react'
import {Table} from 'reactstrap'
export const Tabla1 = () => {
   return(
      <div>
      <Table striped>
      <thead>
        <tr>
          <th>Equipo 1</th>
          <th></th>
          <th>Equipo 2</th>
          <th>Ganador</th>
        </tr>
      </thead>
      <tbody>
        <tr>
         
          <td>Mark</td>
          <td>vs</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
         
          <td>Jacob</td>
          <td>vs</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
        <tr>
         
          <td>Larry</td>
          <td>vs</td>
          <td>the Bird</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </Table>
      </div>
   );
}

export const Tabla2 = () => {
   return(
      <div>
      <Table striped>
      <thead>
        <tr>
          <th>Equipo 1</th>
          <th>vs</th>
          <th>Equipo 2</th>
          <th>Ganador</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>Larry</td>
          <td>the Bird</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </Table>
      </div>
   );
}

export const Calendario = () => {
  return(
     <div>
     <Table striped>
     <thead>
       <tr>
         <th></th>
         <th>Disciplina</th>
         <th>Equipo 1</th>
         <th></th>
         <th>Equipo 2</th>
         <th>Lugar</th>
         <th>Hora</th>
       </tr>
     </thead>
     <tbody>
       <tr>
         <th scope="row"></th>
         <td>Futbol</td>
         <td>Otto</td>
         <td>vs</td>
         <td>@mdo</td>
         <td>@mdo</td>
         <td>@mdo</td>
       </tr>
       <tr>
         <th scope="row"></th>
         <td>Basket</td>
         <td>Thornton</td>
         <td>vs</td>
         <td>@mdo</td>
         <td>@mdo</td>
         <td>@mdo</td>
       </tr>
       <tr>
         <th scope="row"></th>
         <td>Voleibol</td>
         <td>the Bird</td>
         <td>vs</td>
         <td>@mdo</td>
         <td>@mdo</td>
         <td>@mdo</td>
       </tr>
       <tr>
         <th scope="row"></th>
         <td>Tochito</td>
         <td>Otto</td>
         <td>vs</td>
         <td>@mdo</td>
         <td>@mdo</td>
         <td>@mdo</td>
       </tr>
       <tr>
         <th scope="row"></th>
         <td>Natación</td>
         <td>Otto</td>
         <td>vs</td>
         <td>@mdo</td>
         <td>@mdo</td>
         <td>@mdo</td>
       </tr>
     </tbody>
   </Table>
     </div>
  );
}