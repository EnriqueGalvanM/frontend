import React from 'react';
import { Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import { Link } from "react-router-dom"

const ButtonHome = (props) => {
  return (
            <Button><Link to="/home" className="nav-link">Continue</Link></Button>
  );
};

export default ButtonHome;
