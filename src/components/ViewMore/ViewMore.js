import React from 'react';
import './ViewMore.css'
import { CardColumns} from 'reactstrap';
import Preview from '../Preview/Preview';
const ViewMore =(props) => {
      return (
         <div className="cubo">
         <CardColumns>
            {props.HD.map((p) => (
               <Preview key={p.id} src={p.src}/>
            ))}
         </CardColumns>
         </div>
      );
}

export default ViewMore;