import React from 'react'

export default class Loading {
    static load(cb){
        setTimeout(cb, 3000)
    }
}
