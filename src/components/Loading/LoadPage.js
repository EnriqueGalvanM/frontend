import React, { Component } from 'react'
import './LoadPage.css'
import {Spinner} from 'reactstrap'

export class LoadPage extends Component {
    render() {
        return (
            <div className='loadpage'>
                <Spinner className='spin' style={{ width: '3rem', height: '3rem' }} color='danger'/>
                <p>Loading Please Wait...</p>
            </div>
        )
    }
}

export default LoadPage
