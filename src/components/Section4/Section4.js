import React, { Component } from 'react';
import { Tabla1, Tabla2 } from '../Tables/Tables';

class Section4 extends Component {
   render() {
      return (
         <div className="row mt-5">
            <div className="col-lg-6">
               <h4>Resultados</h4>
               <Tabla1/>
            </div>
            <div className="col-lg-6">
            <h4>Partidos</h4>
               <Tabla2/>
            </div>
         </div>
      );
   }
}

export default Section4;