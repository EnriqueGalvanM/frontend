import React from 'react';
import {Card, CardBody, CardImg} from 'reactstrap';
const Preview = ({src}) => {
      return (
         <div>
         <Card>
            <CardBody>
               <CardImg top width="100%" src={src}/>
            </CardBody>
         </Card>
         </div>
      );
}

export default Preview;