import React, { Component } from 'react'
import VideoCover from 'react-video-cover'
import Soft from './video.mp4'
import './video.css'    
import ButtonHome from './../ButtonHome/ButtonHome'
import FadeIn from 'react-fade-in';

class MinimalCoverExample extends Component {
    render() {
        return (
            <div className='video'>
                    <div className='contenido'>
                    <FadeIn delay='10500' transitionDuration='700'>
                        <h1 className='header'>Softtekiada!</h1>
                        <ButtonHome/>
                    </FadeIn>
                    </div>
                <video
                    autoPlay
                    muted
                    style={{
                        objectFit: 'cover',
                        overflow: 'hidden',
                        width: '100%',
                        height: '100%',
                    }}
                    src={Soft}
                />

            </div>
        );
    }
}

export default MinimalCoverExample