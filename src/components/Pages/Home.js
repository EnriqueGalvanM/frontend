import React, { Component } from 'react';
import Layout from '../Layout/Layout';
import Carousell from '../Carousell/Carousell';
import Section1 from '../Section1/Section1';
import Section2 from '../Section2/Section2';
import Section3 from '../Section3/Section3'
import Section4 from '../Section4/Section4';

class Home extends Component {
   render() {
      return (
         <Layout>
            <div>
               <div>
                  <Carousell/>
               </div>
               <Section1/>
               <Section2/>
               <Section3/>
               <Section4/>
            </div>
         </Layout>
      );
   }
}
export default Home;