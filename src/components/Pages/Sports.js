import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import Layout from '../Layout/Layout'
import Disc from '../Disciplines/Disc';


export class Sports extends React.Component{
    render() {
        const src = '/images/wireframe/white-image.png'
        return (
            <Layout>
                <Jumbotron fluid>
                    <Container fluid>
                        <h1 className="display-3">Disciplines</h1>
                        <Disc/>
                    </Container>
                </Jumbotron>
            </Layout>
        )
    }
}

export default Sports
