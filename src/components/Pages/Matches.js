import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import Layout from './../Layout/Layout'
import { Tabla1, Calendario } from '../Tables/Tables';

export class Matches extends React.Component {
    render() {
        return (
            <Layout>
                <Jumbotron fluid>
                    <Container fluid>
                        <h1 className="display-3">Matches</h1>
                        <div className="row">
                            <div className="col-lg-6">
                                <h6>Disciplina 1</h6>
                                <Tabla1/>
                            </div>
                            <div className="col-lg-6">
                                <h6>Disciplina 2</h6>
                                <Tabla1/>
                            </div>
                            <div className="col-lg-6">
                            <h6>Disciplina 3</h6>
                                <Tabla1/>
                            </div>
                            <div className="col-lg-6">
                            <h6>Disciplina 4</h6>
                                <Tabla1/>
                            </div>
                            <div className="col-lg-6">
                            <h6>Disciplina 5</h6>
                                <Tabla1/>
                            </div>
                            <div className="col-lg-6">
                            <h6>Disciplina 6</h6>
                                <Tabla1/>
                            </div>
                            <div className="col-lg-12">
                            <h1>Calendario</h1>
                                <Calendario/>
                            </div>
                        </div>
                    </Container>
                </Jumbotron>
            </Layout>
        )
    }
}

export default Matches
