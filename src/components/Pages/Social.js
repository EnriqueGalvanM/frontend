import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import Layout from './../Layout/Layout'

export class Social extends React.Component{
    render() {
        return (
            <Layout>
                <Jumbotron fluid>
                    <Container fluid>
                        <h1 className="display-3">Social Networks</h1>
                    </Container>
                </Jumbotron>
            </Layout>
        )
    }
}

export default Social
