import React from 'react';
import { Jumbotron, Container } from 'reactstrap';
import Layout from './../Layout/Layout'
import CountryCard from './../CountryCard/CountryCard'
import ButtonHome from './../ButtonHome/ButtonHome'

export class Countries extends React.Component{
    render() {
        return (
            <Layout>
                <Jumbotron fluid>
                    <Container fluid>
                        <h1 className="display-3">Countries</h1>
                    </Container>
                    <ButtonHome/>
                    </Jumbotron>
                <CountryCard/>

            </Layout>
        )
    }
}

export default Countries
