import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './footer.css'

class Footer extends Component {
   render() {
      return (
         <footer id="sticky-footer" className="py-4 bg-dark text-white-50">
         <div className="container text-center">
           <small>Copyright &copy; Your Website</small>
         </div>
       </footer>
      );
   }
}

export default Footer;