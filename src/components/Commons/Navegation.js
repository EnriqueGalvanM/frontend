import React, { Component } from 'react';
import { Link } from "react-router-dom"


class Navegation extends Component {
   render() {
      return (
         <nav className="navbar navbar-expand navbar-light bg-light">
         <div className="container">
         <Link className="navbar-brand" to="/">Softtekiada</Link>
            <div className="collapse navbar-collapse">
         <ul className="navbar-nav ml-auto">
         <li className="nav-item"><Link to="/historia" className="nav-link">History</Link></li>
         <li className="nav-item"><Link to="/sports" className="nav-link">Sports</Link></li>
         <li className="nav-item"><Link to="/partidos" className="nav-link">Matches</Link></li>
            <li className="nav-item"><Link to="/galeria" className="nav-link">Gallery</Link></li>
            <li className="nav-item"><Link to="/countries" className="nav-link">Countries</Link></li>
         </ul>
         <button type="button" className="btn btn-outline-warning"><Link to='/login'>Login</Link></button>
            </div>
         </div>  
         </nav>
      );
   }
}

export default Navegation;