import React, { Component } from 'react';
import Esfera1 from '../Esfera/Esfera1';
import ViewMore from '../ViewMore/ViewMore';

class Section1 extends Component {
   render() {
      const HD = [
         {id:1, src: require('./../../assets/img/004.png')},
         {id:2, src: require('./../../assets/img/004.png')},
         {id:3, src: require('./../../assets/img/004.png')},
         {id:4, src: require('./../../assets/img/004.png')},
         {id:5, src: require('./../../assets/img/004.png')},
         {id:6, src: require('./../../assets/img/004.png')},
         {id:7, src: require('./../../assets/img/004.png')},
         {id:8, src: require('./../../assets/img/004.png')},
         {id:9, src: require('./../../assets/img/004.png')}
       ]
      return (
         <div className="row">
            <div className="col-lg-6">
               <Esfera1/>
            </div>
            <div className="col-lg-6">
               <ViewMore HD={HD}/>
            </div>    
         </div>
      );
   }
}

export default Section1;