import React, { Component, Fragment} from 'react';
import Navegation from '../Commons/Navegation';
import Footer from '../Commons/Footer';

class Layout extends Component {
   render() {
      return (
         <Fragment>
            <Navegation/>
               <div>
                  {this.props.children}
               </div>
               <Footer/>
         </Fragment>
      );
   }
}

export default Layout;