import React, { Component } from 'react';
import { Mosaico, Mosaico3, Mosaico2, Mosaico1, Mosaico4, Mosaico5, Mosaico6, Mosaico7, Mosaico8, Mosaico9 } from '../Mosaico/Mosaico';

class Section3 extends Component {
   render() {
      return (
         <div className="row mt-5">
            <Mosaico1/>
            <Mosaico2/>
            <Mosaico3/>
            <Mosaico4/>
            <Mosaico5/>
            <Mosaico6/>
            <Mosaico7/>
            <Mosaico8/>
            <Mosaico9/>
         </div>
         
      );
   }
}

export default Section3;