import React, { Component } from 'react';
import Esfera1 from '../Esfera/Esfera1';
import {Mision, Vision} from '../Imagenes/Imagenes'
class Section2 extends Component {
   render() {
      return (
         <div className="row">
            <div className="col-lg-6 mt-5">
              <Mision/>
            </div>
            <div className="col-lg-6">
               <Esfera1/>
            </div>
            <div className="col-lg-6">
               <Esfera1/>
            </div>
            <div className="col-lg-6 mt-5">
               <Vision/>
            </div>
         </div>
      );
   }
}

export default Section2;