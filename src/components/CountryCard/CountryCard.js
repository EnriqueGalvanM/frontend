import React from 'react';
import { Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';

const CountryCard = (props) => {
  return (
    <Row>
      <Col sm="6">
        <Card body>
        <img width="100%" src="https://facebook.github.io/create-react-app/img/logo-og.png" alt="Card image cap" />
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
      <Col sm="6">
        <Card body>
        <img width="100%" src="https://facebook.github.io/create-react-app/img/logo-og.png" alt="Card image cap" />
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
      <Col sm="6">
        <Card body>
        <img width="100%" src="https://facebook.github.io/create-react-app/img/logo-og.png" alt="Card image cap" />
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
      <Col sm="6">
        <Card body>
        <img width="100%" src="https://facebook.github.io/create-react-app/img/logo-og.png" alt="Card image cap" />
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
      <Col sm="6">
        <Card body>
        <img width="100%" src="https://facebook.github.io/create-react-app/img/logo-og.png" alt="Card image cap" />
          <CardTitle>Special Title Treatment</CardTitle>
          <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
    </Row>
    
  );
};

export default CountryCard;
