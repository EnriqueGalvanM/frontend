import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './Login.css'
import { Input, Button } from 'reactstrap';

export class Log extends Component {
    render() {
        return (
           <div className='main col-md-6'>
                    <div className="login">
                        <form>
                            <div className="form-group">
                                <label>Email:</label>
                                <Input type="text" className="form-control" placeholder="User Name"/>
                            </div>
                            <div className="form-group">
                                <label>Password:</label>
                                <Input type="password" className="form-control" placeholder="Password"/>
                            </div>
                            <Button type="submit" className="btn btn-secondary">Register</Button>
                        </form>
                    </div>        
           </div>
        )
    }
}

export default Log